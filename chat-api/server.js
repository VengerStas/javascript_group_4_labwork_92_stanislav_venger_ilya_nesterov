const express = require('express');
const app = express();
const expressWs = require('express-ws')(app);
const mongoose = require('mongoose');
const config = require('./config');
const cors = require ('cors');

const users = require('./app/user');
const chat = require('./app/chat');
const messages = require('./app/messages');

app.use(cors());
app.use(express.json());

const port = 8000;

mongoose.connect(config.dbUrl, config.mongoOption).then(() => {
    app.use('/users', users);
    app.use('/chat', chat);
    app.use('/messages', messages)
});



app.listen(port, () => {
    console.log(`Server started on ${port} port`);
});