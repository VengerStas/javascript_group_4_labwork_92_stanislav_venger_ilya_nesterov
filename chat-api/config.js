
module.exports = {
    dbUrl: 'mongodb://localhost/chat',
    mongoOption: { useNewUrlParser: true, useCreateIndex: true}
};