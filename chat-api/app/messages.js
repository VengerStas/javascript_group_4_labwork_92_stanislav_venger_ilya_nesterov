const express = require('express');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const Messages = require('../models/Messages');

const router = express.Router();

router.delete('/:id', [auth, permit("moderator")], (req, res) => {
    Messages.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});

module.exports = router;