const mongoose = require('mongoose');
const nanoid = require('nanoid');

const config = require('./config');

const User = require('./models/Users');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOption);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for ( let collection of collections) {
    await collection.drop();
  }

  await User.create(
    {username: "John", password: '123', role: 'moderator', token: nanoid()}
  );


  return connection.close();
};

run().catch(error => {
  console.log(error)
});