import React, {Fragment} from 'react';
import {Badge, Button, NavItem} from "reactstrap";

const UserMenu = ({user, logout}) => (
  <Fragment>
      <Badge style={{fontSize: '15px'}} pill color="light">Hello, {user.username}</Badge>
    <NavItem>
      <Button style={{padding: '0 10px'}} outline color="primary" onClick={logout}>Log out</Button>
    </NavItem>
  </Fragment>
);

export default UserMenu;