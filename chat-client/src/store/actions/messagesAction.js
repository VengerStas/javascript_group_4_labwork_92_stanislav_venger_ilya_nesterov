import {NotificationManager}  from 'react-notifications';
import {push} from "connected-react-router";
import axios from '../../axios-api';

export const deleteMessages = message => {
    return (dispatch) => {
        return axios.delete('/messages/' + message).then(() => {
            NotificationManager.success('Message has been deleted');
            dispatch(push('/'));
        })
    }
};